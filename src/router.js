import Vue from 'vue'
import Router from 'vue-router'
import Register from './views/Register.vue'
import Login from './views/Login.vue'
import Home from './views/Home.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/notAllowed',
      name: 'notAllowed',
      component: Register
    },
    {
      path: '/',
      name: 'login',
      component: Login
    },
    
  ]
})
