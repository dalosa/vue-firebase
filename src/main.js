import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import firebase from "firebase"
Vue.config.productionTip = false
var firebaseConfig = {
  apiKey: "AIzaSyDZtt_2Q6Y_4JXnOR1rQuLt6ZRRoyUAFKg",
  authDomain: "todo-fire-1a299.firebaseapp.com",
  databaseURL: "https://todo-fire-1a299.firebaseio.com",
  projectId: "todo-fire",
  storageBucket: "todo-fire.appspot.com",
  messagingSenderId: "845896423712",
  appId: "1:845896423712:web:4a3505d507eabb18"
};
firebase.initializeApp(firebaseConfig);


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
